module.exports = {
    "extends": "standard",
    "rules": {
        "prefer-const": "error",
        "no-var": "error"
    }
};